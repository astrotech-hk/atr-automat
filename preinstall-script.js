const process = require('child_process');

// 安装rimraf全局依赖
const installRimraf = () => new Promise((resolve, reject) => {
  process.exec('npm install -g rimraf', error => {
    if (error) {
      reject('安装rimraf失败');
    } else {
      resolve('安装rimraf成功');
    }
  });
})

// 删除package-lock.json
const deletePackageLock = () => new Promise((resolve, reject) => {
  process.exec('rimraf package-lock.json', error => {
    if (error) {
      reject('删除package-lock.json失败');
    } else {
      resolve('删除package-lock.json成功');
    }
  })
})

// 删除node_modules文件夹
const deleteNodeModules = () => new Promise((resolve, reject) => {
  process.exec('rimraf node_modules', error => {
    if (error) {
      reject('删除node_modules失败');
    } else {
      resolve('删除node_modules成功');
    }
  })
})

// 清除npm缓存
const cleanNpmCache = () => new Promise((resolve, reject) => {
  process.exec('npm cache clean --force', error => {
    if (error) {
      reject('清除npm缓存失败');
    } else {
      resolve('清除npm缓存成功');
    }
  })
})

const startTasks = async () => {
  await installRimraf();
  await deletePackageLock();
  try {
    await deleteNodeModules();
  } catch (error) {}
  await cleanNpmCache();
}

startTasks();
